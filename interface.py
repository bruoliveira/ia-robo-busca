from PyQt4.QtGui import * 
from PyQt4.QtCore import *
from robo import Robo
from terreno import Terreno
import sys

cores = {
	0 : QColor(144, 209, 78), #verde
	1 : QColor(148, 137, 79), #marrom
	2 : QColor(81, 140, 209), #azul
	3 : QColor(227, 107, 10) #vermelho
}

class InterfaceQt(object):
	
	linhas = 0
	colunas = 0
	raio = 0
	tabela = None
	app = None
	terreno = None

	def __init__(self, arq_terreno, pos_inicial, pos_final):
		super(InterfaceQt, self).__init__()
		self.pos_inicial = pos_inicial
		self.pos_final = pos_final
		
		# Criacao da aplicacao e da tabela
		self.app = QApplication(sys.argv)
		self.tabela = QTableWidget()

		self.tabela.setWindowTitle("Busca")

		self.terreno = Terreno(arq_terreno)

		self.linhas, self.colunas = self.terreno.dimensoes()
		
		self.tabela.setRowCount(self.linhas)
		self.tabela.setColumnCount(self.colunas)

		header = self.tabela.horizontalHeader()
		vheader = self.tabela.verticalHeader()

		# configurar para fixo o modo das colunas e linhas
		header.setResizeMode(QHeaderView.Fixed)
		vheader.setResizeMode(QHeaderView.Fixed)
		vheader.setVisible(False)
		header.setVisible(False)

		# redimensionar cada coluna e linha para tamanho 7
		for i in range(self.linhas):
			header.resizeSection(i,15)
		for i in range(self.colunas):
			vheader.resizeSection(i,15)

		# calcular e definir a largura da tabela (e consequentemente, da janela)
		width = header.length() + 5
		if self.tabela.verticalScrollBar().isVisible():
			width += self.tabela.verticalScrollBar().width()
		width += self.tabela.frameWidth() * 2
		self.tabela.setFixedWidth(width)

		# calcular e definir a altura da tabela (e consequentemente, da janela)
		height = vheader.length() + 5
		if self.tabela.horizontalScrollBar().isVisible():
			height += self.tabela.horizontalScrollBar.height()
		self.tabela.setFixedHeight(height)

		#inicializar as celulas
		for i in range(self.linhas):
			for j in range(self.colunas):
				self.tabela.setItem(i,j, QTableWidgetItem(""))
				self.tabela.item(i,j).setFont(QFont("Verdana", 15))
				self.tabela.item(i,j).setBackground(cores[int(self.terreno.matriz[i][j])])

		self.tabela.setEditTriggers(QAbstractItemView.NoEditTriggers)
		self.tabela.show()


	def executa(self):

		r2d2 = Robo(self.pos_inicial, self.pos_final, self.terreno.matriz, self.app, self.tabela)
		pos = r2d2.busca_largura()
		self.app.processEvents()

		return self.app.exec_()


class BorderItemDelegate(QStyledItemDelegate):
	def __init__(self, parent, borderRole):
		super(BorderItemDelegate, self).__init__(parent)
		self.borderRole = borderRole

	def paint(self, painter, option, index):
		pen = index.data(self.borderRole).toPyObject()
		rect = QRect(option.rect)
		if pen is not None:
			width = max(pen.width(), 1)
			option.rect.adjust(width, width, -width, -width)      

		super(BorderItemDelegate, self).paint(painter, option, index)

		if pen is not None:
			painter.save()
			painter.setClipRect(rect, Qt.ReplaceClip);          
			pen.setWidth(2 * width)
			painter.setPen(pen)
			painter.drawRect(rect)     
			painter.restore()