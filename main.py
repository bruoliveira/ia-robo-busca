from interface import InterfaceQt
import argparse
import json

def main():
	arquivo_config = args.input[0]
	arquivo_terreno = args.dados[0]

	with open(arquivo_config) as data_file:
		entrada = json.load(data_file)
	pos_inicial = [entrada['pos_inicial']['lin'],entrada['pos_inicial']['col']]
	pos_final = [entrada['pos_final']['lin'],entrada['pos_final']['col']]
	
	interface = InterfaceQt(arquivo_terreno, pos_inicial, pos_final)
	interface.executa()

if __name__ == '__main__':
	parser = argparse.ArgumentParser(description='Ant Clustering')
	parser.add_argument('-i', dest='input', action='store', nargs=1, help='Arquivo de entrada (configuracoes)')
	parser.add_argument('-d', dest='dados', action='store', nargs=1, help='Arquivo de dados')

	args = parser.parse_args()

	main()