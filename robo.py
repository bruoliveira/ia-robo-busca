import random
import time
from PyQt4.QtGui import *
from PyQt4.QtCore import *
from treelib import Node, Tree
import interface as iface

valores = {
	0 : 1,
	1 : 5,
	2 : 10,
	3 : 15
}

cores = {
	0 : QColor(232, 232, 16), #amarelo - caminho
	1 : QColor(0,0,0), #preto - visitados
	2 : QColor(226, 24, 203) #lilas - borda
}

class Arvore(object):

	raiz = None
	adjacentes = []
	coord = []
	marcado = False

	def __init__(self):
		super(Arvore, self).__init__()


class Robo(object):

	pos = []
	pos_inicial = []
	pos_final = []
	visitados = []
	expandidos = []
	terreno = None
	arvore = None

	def __init__(self, pos_inicial, pos_final, terreno, qt_app, qt_tabela):
		super(Robo, self).__init__()
		self.pos_inicial = self.pos = pos_inicial
		self.pos_final = pos_final
		self.max_linhas = len(terreno)
		self.max_colunas = len(terreno[0])
		self.terreno = terreno
		self.qt_app = qt_app
		self.qt_tabela = qt_tabela

	def expandir(self, nodo):
		posicoes = {
			0 : [(self.pos[0] - 1), (self.pos[1])    ], #norte
			1 : [(self.pos[0])    , (self.pos[1] + 1)], #leste
			2 : [(self.pos[0] + 1), (self.pos[1])    ], #sul
			3 : [(self.pos[0])    , (self.pos[1] - 1)]  #oeste
		}
		
		nodo.tag = self.pos
		for i in range(0,4):
			novo_nodo = Node()
			if posicoes[i][0] >= 0 and posicoes[i][0] < self.max_linhas and posicoes[i][1] >= 0 and posicoes[i][1] < self.max_colunas and posicoes[i] not in self.expandidos:
				novo_nodo.tag = posicoes[i]
				novo_nodo.data = self.terreno[posicoes[i][0]][posicoes[i][1]]
				self.qt_tabela.item(*posicoes[i]).setForeground(cores[2])
				self.qt_tabela.item(*posicoes[i]).setText("*")
				self.qt_app.processEvents()
				self.arvore.add_node(novo_nodo, nodo.identifier)



	
	def busca_largura(self):
		MyBorderRole = 33
		pen = QPen(cores[1])
		pen.setWidth(2)
		
		self.arvore = Tree()
		self.arvore.create_node('', data=self.terreno[self.pos[0]][self.pos[1]])
		
		self.expandir(self.arvore.get_node(self.arvore.root))
		nodo_atual = self.arvore.get_node(self.arvore.root)
		
		self.expandidos.append(nodo_atual.tag)
		self.visitados.append(nodo_atual)

		backtrack = []
		#self.qt_tabela.item(*self.pos_inicial).setForeground(QColor(255,255,255))
		self.qt_tabela.item(*self.pos_inicial).setText("*")
		self.qt_app.processEvents()
		
		while len(self.visitados) != 0:
			nodo_atual = self.visitados.pop(0)
			for i in nodo_atual.fpointer:
				w = self.arvore.get_node(i)
				if w.tag not in self.expandidos:
					time.sleep(0.01)
					self.pos = w.tag
					self.expandir(w)
					self.qt_tabela.item(*self.pos).setForeground(cores[1])
					#self.qt_tabela.item(*w.tag).setText("*")
					self.qt_app.processEvents()
					if self.pos == self.pos_final:
						custo = 0
						while w.bpointer:
							backtrack.append(w)
							w = self.arvore.get_node(w.bpointer)
						backtrack.append(w)

						for i in reversed(backtrack):
							time.sleep(0.1)
							custo += valores[int(self.terreno[i.tag[0]][i.tag[1]])]
							self.qt_tabela.item(*i.tag).setData(MyBorderRole, pen)
							delegate = iface.BorderItemDelegate(self.qt_tabela, MyBorderRole) 
							self.qt_tabela.setItemDelegate(delegate)
							self.qt_app.processEvents()
						
						#Fim da execucao
						print "------------------------"
						print "- Custo: %d" % custo
						print "------------------------"
						return reversed(backtrack)
					self.visitados.append(w)
					self.expandidos.append(w.tag)

			#self.visitados.pop()

	def busca_profundidade(self):
		pass

	def busca_custo_uniforme(self):
		pass
